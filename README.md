# README #
get-next-working-day is a spring boot microservice application. please find below important information about this microservice:

1. User need to enter the date in the format yyyy-mm-dd and next-working-day API will respond with next working day. 
2. if User enters invalid date or invalid date format then service respond with http response code 400 and a message informing 
   that user input date is not valid date.
3. This API will call third party external source(get-third-party-bank-off microservice) to fetch the list of bank Holidays. 
   third party API can return the bank Holiday list based on bank name and allowed bank names are COOP and BARCLAYS.     
3. Default port is 4444 as defined in application.properties file & and has key/value pair defined for the third party service call and 
   port(5555 default port for get-third-party-bank-off application). Need to define Bank Name as COOP or BARCLAYS here as well. Default is COOP.
   
get-next-working-day is a spring boot microservice application. please find below important information about this microservice:

1. This application API bankOffDays is a RESTFul webservice API which needs user input for bankName(allowed values are COOP or BARCLAYS). 
2. if user enters incorrect input then receives response with 400 http response code and error message - User input date is invalid date.
3. Default port is defined in the application.properties as 5555.


Important points: 

1. get-next-working-day application internally calls a another spring boot microservice application "get-third-party-bank-off" using RESTFul webservice 
   call. "get-third-party-bank-off" microservice application provides specific bank Holidays based input bank name. I have setup 2 banks COOP & BARCLAYS 
   for demostration purpose so based on the input parameter bankName, get-third-party-bank-off application returns holiday list for input bank 
   and those Holidays will be used by the get-next-working-day microservice to finally identify the next working date.

    
 Step to Build and run above 2 microservice applications:
 get-next-working-day application:
 1. run "mvn clean install" command in CMD in the project folder get-next-working-day having the pom files.
 2. import this application as existing mvn project in STS or Eclipse and then right click NextBusinessDayApplication java file and run as 
    Java application. see in the console if application deploys successfully in inbuilt tomcat server at port 4444. for any error, please check the 
	error and connect with me.
	
get-third-party-bank-off application:
1. run "mvn clean install" command in CMD in the project folder get-third-party-bank-off having the pom files.
2. import this application as existing mvn project in STS or Eclipse and then right click ThirdPartyBankOffApplication java file and run as 
    Java application. see in the console if application deploys successfully in inbuilt tomcat server at port 5555. for any error, please check the 
	error and connect with me.

Implemented Swagger documents for both microservice applications and Swagger descriptions are self explanatory. 
Swagger can be accessed at the below URLs:

http://localhost:4444/swagger-ui.html

http://localhost:5555/swagger-ui.html

Please use "try it out" option in the Swagger documentation itself to test/review the applications responses easily:

Few scenarios: 
Defined sample bankCOOPHolidayList=[2020-01-15, 2020-01-17, 2020-04-29, 2020-04-27, 2020-05-04, 2020-05-05, 2020-05-06]

--------------------------------------
Request:  http://localhost:4444/digital/bank/next-working-day?after=2020-05-01

response: 
{
  "nextWorkingDate": "2020-05-07",
  "nextWorkingDay": "Thursday",
  "nextWorkingDesc": "The next working day"
}
--------------------------------------
--------------------------------------
Request:  http://localhost:4444/digital/bank/next-working-day?after=2020-04-28

Response:
{
  "nextWorkingDate": "2020-04-30",
  "nextWorkingDay": "Thursday",
  "nextWorkingDesc": "The next working day"
}
--------------------------------------
--------------------------------------
Request:  http://localhost:4444/digital/bank/next-working-day?after=2020%2F04%2F32

Response:
{
  "message": "The after date is not a valid date",
  "code": "400"
}
--------------------------------------

