package coopbank.digital.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Vinay Agrawal 
 * 
 * Spring Boot microservice application to respond the
 * next working date based on input day
 *
 */
@SpringBootApplication
public class NextBusinessDayApplication {

	public static void main(String[] args) {
		SpringApplication.run(NextBusinessDayApplication.class, args);
	}
}
