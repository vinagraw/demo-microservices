package coopbank.digital.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * @author Vinay Agrawal
 * Configuration class to externalise third party endpoint parameters
 *
 */
@Component
@ConfigurationProperties
@Validated
public class ThirdPartyConfig {

	private String urlPort;
	private String urlIPAddress;
	private String thirdPartyEndpoint;
	private String bankName;

	public String getUrlPort() {
		return urlPort;
	}
	public void setUrlPort(String urlPort) {
		this.urlPort = urlPort;
	}
	public String getUrlIPAddress() {
		return urlIPAddress;
	}
	public void setUrlIPAddress(String urlIPAddress) {
		this.urlIPAddress = urlIPAddress;
	}
	public String getThirdPartyEndpoint() {
		return thirdPartyEndpoint;
	}
	public void setThirdPartyEndpoint(String thirdPartyEndpoint) {
		this.thirdPartyEndpoint = thirdPartyEndpoint;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	@Override
	public String toString() {
		return "GlobalProperties{" + "ThirdPartyURLPORT=" + urlPort + ", ThirdPartyURLIPAddress='" + urlIPAddress + '\''
				+ '}';
	}
}