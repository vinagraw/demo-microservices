package coopbank.digital.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import coopbank.digital.demo.model.NextBusinessDayResponseDTO;
import coopbank.digital.demo.process.GetNextWorkingDayProcess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/digital/bank")
@Api(value = "Working Day Application")
public class NextBusinessDayController {

	@Autowired
	GetNextWorkingDayProcess getNextWorkingDayProcess;

	@ApiOperation(value = "The Next working Day", response = NextBusinessDayResponseDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The next working day"),
			@ApiResponse(code = 400, message = "The after date is not a valid date") })
	@GetMapping("/next-working-day")
	public Object getNextBusinessDay(
			@ApiParam(value = "The date from which to find the _next_ working day format allowed yyyy-MM-dd", required = false) @RequestParam(name = "after", required = false) String date) {
		return getNextWorkingDayProcess.findNextWorkingDate(date);
	}
}
