package coopbank.digital.demo.model;

import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Third Party Endpoint call for fetching COOP or Barclay Bank Holiday List
 * Response Object
 * 
 * @author Vinay Agrawal
 *
 */
@ApiModel(description = "Bank Holidays date list")
public class BankHolidaysResponseDTO {

	@ApiModelProperty(notes = "COOP Bank Holiday List")
	List<String> bankCOOPHolidayList;

	@ApiModelProperty(notes = "Barclays Bank Holiday List")
	List<String> bankBarclaysHolidayList;

	@ApiModelProperty(notes = "Bank Description")
	String bankDescription;

	public BankHolidaysResponseDTO(List<String> bankCOOPHolidayList, List<String> bankBarclaysHolidayList,
			String bankDescription) {
		super();
		this.bankCOOPHolidayList = bankCOOPHolidayList;
		this.bankBarclaysHolidayList = bankBarclaysHolidayList;
		this.bankDescription = bankDescription;
	}

	public BankHolidaysResponseDTO() {
		super();
	}

	public List<String> getBankCOOPHolidayList() {
		return bankCOOPHolidayList;
	}

	public void setBankCOOPHolidayList(List<String> bankCOOPHolidayList) {
		this.bankCOOPHolidayList = bankCOOPHolidayList;
	}

	public List<String> getBankBarclaysHolidayList() {
		return bankBarclaysHolidayList;
	}

	public void setBankBarclaysHolidayList(List<String> bankBarclaysHolidayList) {
		this.bankBarclaysHolidayList = bankBarclaysHolidayList;
	}

	public String getBankDescription() {
		return bankDescription;
	}

	public void setBankDescription(String bankDescription) {
		this.bankDescription = bankDescription;
	}

	@Override
	public String toString() {
		return "BnakHolidaysResponseDTO [bankCOOPHolidayList=" + bankCOOPHolidayList + ", bankBarclaysHolidayList="
				+ bankBarclaysHolidayList + ", bankDescription=" + bankDescription + "]";
	}
}
