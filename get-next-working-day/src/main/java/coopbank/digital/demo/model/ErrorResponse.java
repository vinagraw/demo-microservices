package coopbank.digital.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Error response class
 * 
 * @author Vinay Agrawal
 *
 */
public class ErrorResponse {
	
	@JsonProperty
	private String message;
	@JsonProperty
	String code;
	
	public ErrorResponse(String message, String code) {
		super();
		this.message = message;
		this.code = code;
	}

	@Override
	public String toString() {
		return "ErrorResponse [message=" + message + ", code=" + code + "]";
	}
}
