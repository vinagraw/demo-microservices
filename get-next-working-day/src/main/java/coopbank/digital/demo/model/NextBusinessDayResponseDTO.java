package coopbank.digital.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="Next working day Response Object")
public class NextBusinessDayResponseDTO {
	
	@JsonProperty
	@ApiModelProperty(notes = "The Next working date")
	String nextWorkingDate;
	@JsonProperty
	@ApiModelProperty(notes = "The Next working day")
	String nextWorkingDay;
	@JsonProperty
	@ApiModelProperty(notes = "The next working day description")
	String nextWorkingDesc;
	
	public NextBusinessDayResponseDTO(String nextWorkingDate, String nextWorkingDay, String nextWorkingDesc) {
		super();
		this.nextWorkingDate = nextWorkingDate;
		this.nextWorkingDay = nextWorkingDay;
		this.nextWorkingDesc = nextWorkingDesc;
	}

	@Override
	public String toString() {
		return "NextBusinessDayResponse [nextWorkingDate=" + nextWorkingDate+ ", nextWorkingDay=" + nextWorkingDay
				+ ", nextWorkingDescription=" + nextWorkingDesc+ "]";
	}
}
