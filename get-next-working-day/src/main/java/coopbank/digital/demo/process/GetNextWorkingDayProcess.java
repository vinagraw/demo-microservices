package coopbank.digital.demo.process;

import java.text.ParseException;

/**
 * @author Vinay Agrawal
 *
 */
public interface GetNextWorkingDayProcess {
	
	public Object findNextWorkingDate(String date);
	
	String getNextBusinessDate(String dateStr) throws ParseException;
	
	String getCurrentDate();
	
	boolean isDateValid(String date);
	
	public String findDayBasedOnDate(String dateToCalcDay);
}
