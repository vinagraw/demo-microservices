package coopbank.digital.demo.processimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import coopbank.digital.demo.config.ThirdPartyConfig;
import coopbank.digital.demo.model.BankHolidaysResponseDTO;
import coopbank.digital.demo.model.ErrorResponse;
import coopbank.digital.demo.model.NextBusinessDayResponseDTO;
import coopbank.digital.demo.process.GetNextWorkingDayProcess;
import coopbank.digital.demo.thirdpartyconnection.ThirdPartyConnection;

/**
 * Implementation class for the Next working day business logic Calculates the
 * Next working business day by excluding weekends and Bank Holidays
 *
 * @author Vinay Agrawal
 * 
 *
 */
@Component
public class GetNextWorkingDayProcessImpl implements GetNextWorkingDayProcess {

	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String BANK_COOP = "COOP";
	private static final Integer DAY_SUNDAY = 1;
	private static final Integer DAY_SATURDAY = 7;
	private static final String BAD_REQUEST = "400";
	private static final String RESPONSE_DESCRIPTION = "The next working day";
	private static final String FORMAT_DAY_OF_WEEK = "EEEE";

	@Autowired
	private ThirdPartyConfig thirdPartyConfig;
	@Autowired
	private ThirdPartyConnection thirdPartyConnection;

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT); 

	/*
	 * findNextWorkingDate method has business logic to calculate the next working
	 * date
	 */
	public Object findNextWorkingDate(String date) {
		ErrorResponse errorResponse = null;
		try {
			return ruleToCalcNextBusinessDay(date);
		} catch (Exception exception) {
			errorResponse = new ErrorResponse("Please correct the application request", BAD_REQUEST);
			return new ResponseEntity<Object>(errorResponse, HttpStatus.BAD_REQUEST);
		}
	}

	/*
	 * ruleToCalcNextBusinessDay method defines rules for various scenario to
	 * calculate next business date
	 */
	private Object ruleToCalcNextBusinessDay(String date) throws ParseException {
		NextBusinessDayResponseDTO response;
		String nextWorkingDate;
		ErrorResponse errorResponse;
		// Scenario 1: In case user did not provide date input
		if (date == null) {
			// should consider next date of current date
			nextWorkingDate = getNextBusinessDate(getCurrentDate());
		} else {
			// Scenario 2: In case user enters invalid date then throw exception with http
			// reseponse code 400
			if (!isDateValid(date)) {
				errorResponse = new ErrorResponse("The after date is not a valid date", BAD_REQUEST);
				return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
			}
			// Scenario 3: Calculate the next working date
			nextWorkingDate = getNextBusinessDate(date);
		}
		response = new NextBusinessDayResponseDTO(nextWorkingDate, findDayBasedOnDate(nextWorkingDate),
				RESPONSE_DESCRIPTION);
		return new ResponseEntity<NextBusinessDayResponseDTO>(response, HttpStatus.OK);
	}

	/*
	 * getCurrentDate method - to find out today's date
	 */ @Override
	public String getCurrentDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date date = new Date();
		return dateFormat.format(date);
	}

	@Override
	public String getNextBusinessDate(String dateStr) throws ParseException {
		Date date = null;
		try {
			date = simpleDateFormat.parse(dateStr);
		} catch (ParseException parseException) {
			throw parseException;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_WEEK, 1);
		// third party API call to get bank Holidays
		BankHolidaysResponseDTO bankHolidaysResponseDTO = thirdPartyConnection.thirdPartyServiceCall();
		while (!isWorkingDay(calendar.getTime(), calendar, bankHolidaysResponseDTO)) {
			calendar.add(Calendar.DAY_OF_WEEK, 1);
		}
		return simpleDateFormat.format(calendar.getTime());
	}

	/* isWorkingDay method identifies if given date is working day or not */
	private boolean isWorkingDay(Date date, Calendar calendar, BankHolidaysResponseDTO bankHolidaysResponseDTO) {
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if ((dayOfWeek == DAY_SATURDAY) || (dayOfWeek == DAY_SUNDAY)) {
			return false;
		}
		/*
		 * logic to identify that if we looking Holidays for COOP bank or Barclays Bank
		 * based on the configuration given in application.properties file
		 */
		if (bankHolidaysResponseDTO != null) {
				List<String> bankHolidayList = thirdPartyConfig.getBankName().equals(BANK_COOP)
					? bankHolidaysResponseDTO.getBankCOOPHolidayList()
					: bankHolidaysResponseDTO.getBankBarclaysHolidayList();
			if (bankHolidayList.contains(simpleDateFormat.format(calendar.getTime()))) {
				return false;
			}
		}
		return true;
	}

	/* isDateValid is to validate date format and if date entered is currect */
	@Override
	public boolean isDateValid(String dateToValidate) {
		simpleDateFormat.setLenient(false);
		try {
			simpleDateFormat.parse(dateToValidate);
			return true;
		} catch (ParseException parseException) {
			return false;
		}

	}

	/* findDayBasedOnDate method used to find the day based on input date */
	@Override
	public String findDayBasedOnDate(String dateToCalcDay) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date dateToCalculateDay = null;
		try {
			dateToCalculateDay = dateFormat.parse(dateToCalcDay);
		} catch (ParseException parseException) {
			parseException.printStackTrace();
		}
		SimpleDateFormat formatDayOfWeek = new SimpleDateFormat(FORMAT_DAY_OF_WEEK);
		return formatDayOfWeek.format(dateToCalculateDay);
	}

}
