package coopbank.digital.demo.thirdpartyconnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import coopbank.digital.demo.config.ThirdPartyConfig;
import coopbank.digital.demo.model.BankHolidaysResponseDTO;

/**
 * @author Vinay Agrawal
 * 
 *Implementation logic for third party webservice calls
 *
 */
@Component
public class ThirdPartyConnection {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ThirdPartyConfig thirdPartyConfig;

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	/*
	 * thirdPartyServiceCall method calls third party application RESTFul webservice
	 * endpoint to provide Bank holidays
	 */
	public BankHolidaysResponseDTO thirdPartyServiceCall() {
		// third party call testing
		String url = "http://" + thirdPartyConfig.getUrlIPAddress() + ":" + thirdPartyConfig.getUrlPort() + "/"
				+ thirdPartyConfig.getThirdPartyEndpoint() + thirdPartyConfig.getBankName();
		System.out.println(
				"application port: " + thirdPartyConfig.getUrlPort() + " and third party application URL: " + url);
		BankHolidaysResponseDTO bankHolidaysResponseDTO = restTemplate.getForObject(url, BankHolidaysResponseDTO.class);
		System.out.println("Third party API service call - RESPONSE " + bankHolidaysResponseDTO.toString());
		return bankHolidaysResponseDTO;
	}
}
