package coopbank.digital.demotest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import coopbank.digital.demo.config.ThirdPartyConfig;
import coopbank.digital.demo.model.BankHolidaysResponseDTO;
import coopbank.digital.demo.model.NextBusinessDayResponseDTO;
import coopbank.digital.demo.processimpl.GetNextWorkingDayProcessImpl;
import coopbank.digital.demo.thirdpartyconnection.ThirdPartyConnection;

@RunWith(MockitoJUnitRunner.class)
public class GetNextWorkingDayProcessImplTest {

	@InjectMocks
	private GetNextWorkingDayProcessImpl getNextWorkingDayProcessImpl;

	@Mock
	private ThirdPartyConnection thirdPartyConnection;

	@Mock
	private ThirdPartyConfig thirdPartyConfig;

	private final static String DATE_FORMAT = "yyyy-MM-dd";
	private final static String RESPONSE_DESCRIPTION = "The next working day";

	//test the getNextBusinessDate method
	@Test
	public void testGetNextBusinessDate() {
		String expectedCOOPNextWorkingDateForThirdPartyHoliday = "2020-04-28";
		String expectedBarclaysNextWorkingDateNoThirdPartyHoliday = "2020-04-27";
		String actualBusinessDate = null;
		BankHolidaysResponseDTO BankHolidaysResponseDTO = createBankHolidayResponseDTO();
		try {
			when(thirdPartyConnection.thirdPartyServiceCall()).thenReturn(BankHolidaysResponseDTO);

			/*
			 * Scenario 1: should return next working date as 2020-04-28, Tuesday as
			 * 2020-04-27 is defined Holiday in Third party application
			 */
			when(thirdPartyConfig.getBankName()).thenReturn("COOP");
			actualBusinessDate = getNextWorkingDayProcessImpl.getNextBusinessDate("2020-04-24");
			assertEquals(expectedCOOPNextWorkingDateForThirdPartyHoliday, actualBusinessDate);
			/*
			 * Scenario 2: should return next working date as 2020-04-27, Monday as
			 * 2020-04-27 is not defined in Third party application as Holiday for Barclays
			 */
			when(thirdPartyConfig.getBankName()).thenReturn("BARCLAYS");
			when(thirdPartyConnection.thirdPartyServiceCall()).thenReturn(BankHolidaysResponseDTO);
			actualBusinessDate = getNextWorkingDayProcessImpl.getNextBusinessDate("2020-04-24");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertEquals(expectedBarclaysNextWorkingDateNoThirdPartyHoliday, actualBusinessDate);
	}

	//test the findNextWorkingDate method
	@Test
	public void testFindNextWorkingDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		//setting up Barclays Expected Response
		NextBusinessDayResponseDTO nextBarclaysBusinessDayResponseDTO = new NextBusinessDayResponseDTO("2020-04-29",
				getNextWorkingDayProcessImpl.findDayBasedOnDate("2020-04-29"), RESPONSE_DESCRIPTION);
		Object expectedBarclaysResponseEntity = new ResponseEntity(nextBarclaysBusinessDayResponseDTO, HttpStatus.OK);
		//setting up COOP Expected Response
		NextBusinessDayResponseDTO nextCOOPBusinessDayResponseDTO = new NextBusinessDayResponseDTO("2020-04-30",
				getNextWorkingDayProcessImpl.findDayBasedOnDate("2020-04-30"), RESPONSE_DESCRIPTION);
		Object expectedCOOPResponseEntity = new ResponseEntity(nextCOOPBusinessDayResponseDTO, HttpStatus.OK);
		
		BankHolidaysResponseDTO BankHolidaysResponseDTO = createBankHolidayResponseDTO();
		

		/*
		 * Scenario 1: should return next working date as 2020-04-28, Tuesday as
		 * 2020-04-27 is defined Holiday in Third party application
		 */
		when(thirdPartyConfig.getBankName()).thenReturn("COOP");
		when(thirdPartyConnection.thirdPartyServiceCall()).thenReturn(BankHolidaysResponseDTO);
		Object nextWorkingDay = getNextWorkingDayProcessImpl.findNextWorkingDate("2020-04-28");
		assertEquals(expectedCOOPResponseEntity.toString(), nextWorkingDay.toString());
		/*
		 * Scenario 2: should return next working date as 2020-04-27, Monday as
		 * 2020-04-27 is not defined in Third party application as Holiday for Barclays
		 */
		when(thirdPartyConfig.getBankName()).thenReturn("BARCLAYS");
		when(thirdPartyConnection.thirdPartyServiceCall()).thenReturn(BankHolidaysResponseDTO);
		nextWorkingDay = getNextWorkingDayProcessImpl.findNextWorkingDate("2020-04-28");
		assertEquals(expectedBarclaysResponseEntity.toString(), nextWorkingDay.toString());
	}
	
	//test getCurrentDate method
	@Test
	public void testGetCurrentDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date date = new Date();
		String todayDate = dateFormat.format(date);
		String actualDate = getNextWorkingDayProcessImpl.getCurrentDate();
		assertEquals(todayDate, actualDate);
	}

	/*
	 * test method testGetNextWorkingDayProcessImplParseExceptionOccurred to
	 * identify if entered date format is correct or not
	 */
	@Test(expected = ParseException.class)
	public void testGetNextWorkingDayProcessImplParseExceptionOccurred() throws ParseException {
		String actualBusinessDate = getNextWorkingDayProcessImpl.getNextBusinessDate("2020/12/12");
	}
	/*
	 * test method testIsDateValidSuccess to
	 * identify if entered date format is correct or not
	 */
	@Test
	public void testIsDateValidSuccess() {
		boolean expectedValidDate = true;
		boolean actualValidDate = getNextWorkingDayProcessImpl.isDateValid("2020-01-20");
		assertEquals(expectedValidDate, actualValidDate);
	}

	/* test if entered date is valid date */
	@Test
	public void testIfDateValid() {
		boolean expectedDateResult = false;
		boolean ifDateValid = getNextWorkingDayProcessImpl.isDateValid("2020-15-20");
		assertEquals(expectedDateResult, ifDateValid);
	}

	/*unit test for findDayBasedOnDate method*/
	@Test
	public void testFindDayBasedOnDate() {
		String expectedDay = "Monday";
		String actualDay = getNextWorkingDayProcessImpl.findDayBasedOnDate("2020-04-27");
		assertEquals(expectedDay, actualDay);
	}
	
	public BankHolidaysResponseDTO createBankHolidayResponseDTO() {
		BankHolidaysResponseDTO BankHolidaysResponseDTO = new BankHolidaysResponseDTO();

		ArrayList<String> bankCOOPHolidayList = new ArrayList<>();
		bankCOOPHolidayList.add("2020-01-15");
		bankCOOPHolidayList.add("2020-01-17");
		bankCOOPHolidayList.add("2020-04-29");
		bankCOOPHolidayList.add("2020-04-27");

		ArrayList<String> bankBarclaysHolidayList = new ArrayList<>();
		bankBarclaysHolidayList.add("2020-01-18");
		bankBarclaysHolidayList.add("2020-01-19");
		bankBarclaysHolidayList.add("2020-04-30");
		bankBarclaysHolidayList.add("2020-05-01");

		BankHolidaysResponseDTO.setBankCOOPHolidayList(bankCOOPHolidayList);
		BankHolidaysResponseDTO.setBankBarclaysHolidayList(bankBarclaysHolidayList);
		BankHolidaysResponseDTO.setBankDescription("COOP Bank Holiday List");
		return BankHolidaysResponseDTO;
	}
}
