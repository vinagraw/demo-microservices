package coopbank.digital.demotest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import coopbank.digital.demo.controller.NextBusinessDayController;
import coopbank.digital.demo.processimpl.GetNextWorkingDayProcessImpl;

/**
 * @author Vinay Agrawal
 * 
 *         JUnit Test cases for Next Business Controller class methods
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NextBusinessDayControllerTest {

	@InjectMocks
	private NextBusinessDayController nextBusinessDayController;

	@Mock
	private GetNextWorkingDayProcessImpl getNextWorkingDayProcess;

	// test the getNextBusinessDate method
	@Test
	public void testgetNextBusinessDay() {

		String after = "2020-05-01";
		String nextBusinessDayExpected = "2020-05-04";
		when(getNextWorkingDayProcess.findNextWorkingDate(after)).thenReturn("2020-05-04");
		/* call the controller API call */
		Object responseActual = nextBusinessDayController.getNextBusinessDay(after);
		assertEquals(responseActual.toString(), nextBusinessDayExpected);
	}
}
