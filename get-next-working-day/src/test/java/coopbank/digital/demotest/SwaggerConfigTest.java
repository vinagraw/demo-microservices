package coopbank.digital.demotest;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import coopbank.digital.demo.config.SwaggerConfig;
import springfox.documentation.spring.web.plugins.Docket;
/**
 * @author Vinay Agrawal Swagger Config class provides the necessary
 *         confguration requirement for Swagger report generation
 *
 */

@RunWith(MockitoJUnitRunner.Silent.class)
public class SwaggerConfigTest {

	@InjectMocks
	private SwaggerConfig swaggerConfig;

	// Unit test for getAPI method
	@Test
	public void testGetAPI() {

		Docket docket = swaggerConfig.api();
		assertNotNull(docket);
	}
}
