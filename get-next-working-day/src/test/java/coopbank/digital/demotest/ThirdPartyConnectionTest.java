package coopbank.digital.demotest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import coopbank.digital.demo.config.ThirdPartyConfig;
import coopbank.digital.demo.model.BankHolidaysResponseDTO;
import coopbank.digital.demo.processimpl.GetNextWorkingDayProcessImpl;
import coopbank.digital.demo.thirdpartyconnection.ThirdPartyConnection;

/**
 * @author Vinay Agrawal
 * 
 *JUnit Test cases for third party Connection class methods
 *
 */
	@RunWith(MockitoJUnitRunner.class)
	public class ThirdPartyConnectionTest {

		@InjectMocks
		private GetNextWorkingDayProcessImpl getNextWorkingDayProcessImpl;

		@InjectMocks
		private ThirdPartyConnection thirdPartyConnection;

		@Mock
		private ThirdPartyConfig thirdPartyConfig;
		
		@Mock
		private RestTemplate restTemplate;


		//test the getNextBusinessDate method
		@Test
		public void testThirdPartyServiceCall() {
			GetNextWorkingDayProcessImplTest getNextWorkingDayProcessImplTest = new GetNextWorkingDayProcessImplTest();
			BankHolidaysResponseDTO bankHolidaysResponseDTO = getNextWorkingDayProcessImplTest.createBankHolidayResponseDTO();
			String COOP_DESCRIPTION = "COOP Bank Holiday List";
			when(thirdPartyConfig.getUrlIPAddress()).thenReturn("localhost");
			when(thirdPartyConfig.getUrlPort()).thenReturn("5555");
			when(thirdPartyConfig.getThirdPartyEndpoint()).thenReturn("thirdParty/Bank/bankOffDays?bankName=");
			when(thirdPartyConfig.getBankName()).thenReturn("COOP");
			when(restTemplate.getForObject("http://localhost:5555/thirdParty/Bank/bankOffDays?bankName=COOP", BankHolidaysResponseDTO.class)).thenReturn(bankHolidaysResponseDTO);
			
			BankHolidaysResponseDTO bankCOOPHolidaysResponseDTOActual = thirdPartyConnection.thirdPartyServiceCall();
			assertEquals(COOP_DESCRIPTION, bankCOOPHolidaysResponseDTOActual.getBankDescription());
		}
}
