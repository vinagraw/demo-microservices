package coopbank.digital.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Vinay Agrawal 
 * 
 * Spring Boot microservice application to respond the
 * list of the bank Holiday based on the Bank name input
 *
 */

@SpringBootApplication
public class ThirdPartyBankOffApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThirdPartyBankOffApplication.class, args);
	}
}
