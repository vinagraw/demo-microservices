package coopbank.digital.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * @author Vinay Agrawal
 * Configuration class to externalise third party details like Bank Holiday List
 *
 */
@Component
@ConfigurationProperties
@Validated
public class BankHolidayConfig {

	String bankCOOPHolidayList;

	String bankBarclaysHolidayList;

	String bankCOOPHolidaysOffDesc;

	String bankBarclaysHolidaysOffDesc;

	public String getBankCOOPHolidayList() {
		return bankCOOPHolidayList;
	}

	public void setBankCOOPHolidayList(String bankCOOPHolidayList) {
		this.bankCOOPHolidayList = bankCOOPHolidayList;
	}

	public String getBankBarclaysHolidayList() {
		return bankBarclaysHolidayList;
	}

	public void setBankBarclaysHolidayList(String bankBarclaysHolidayList) {
		this.bankBarclaysHolidayList = bankBarclaysHolidayList;
	}

	public String getBankCOOPHolidaysOffDesc() {
		return bankCOOPHolidaysOffDesc;
	}

	public void setBankCOOPHolidaysOffDesc(String bankCOOPHolidaysOffDesc) {
		this.bankCOOPHolidaysOffDesc = bankCOOPHolidaysOffDesc;
	}

	public String getBankBarclaysHolidaysOffDesc() {
		return bankBarclaysHolidaysOffDesc;
	}

	public void setBankBarclaysHolidaysOffDesc(String bankBarclaysHolidaysOffDesc) {
		this.bankBarclaysHolidaysOffDesc = bankBarclaysHolidaysOffDesc;
	}

	@Override
	public String toString() {
		return "GlobalProperties{" + "bankCOOPHolidayList=" + bankCOOPHolidayList + ", bankBarclaysHolidayList='" + bankBarclaysHolidayList + '\''
				+ '}';
	}
}