package coopbank.digital.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Vinay Agrawal
 * Swagger Config class provides the necessary confguration requirement for Swagger report generation
 *
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	private static final String BASE_PACKAGED = "coopbank.digital.demo.controller";
	private static final String SWAGGER_TITLE= "Microservice External Third Party Bank Off Application";
	private static final String SWAGGER_DESCRIPTION="Bank Holiday List App";
	private static final String CONTACT_NAME="Vinay Agrawal";
	private static final String CONTACT_PERSON_WEBSITE="https://www.capgemini.com/";
	private static final String CONTACT_EMAIL="Vinay.Agrawal@capgemini.com";
	private static final String LICENCE="Apache 2.0";
	private static final String LICENCE_URL="http://www.apache.org/licenses/LICENSE-2.0.html";
	private static final String LICENCE_VERSION="1.0.0";
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors
						.basePackage(BASE_PACKAGED))
				.paths(PathSelectors.regex("/.*"))
				.build().apiInfo(thirdPartyHolidayAPIsInfo());
	}

	private ApiInfo thirdPartyHolidayAPIsInfo() {
		return new ApiInfoBuilder().title(SWAGGER_TITLE)
				.description(SWAGGER_DESCRIPTION)
				.contact(new Contact(CONTACT_NAME, CONTACT_PERSON_WEBSITE, CONTACT_EMAIL))
				.license(LICENCE).licenseUrl(LICENCE_URL).version(LICENCE_VERSION)
				.build();
	}
}
