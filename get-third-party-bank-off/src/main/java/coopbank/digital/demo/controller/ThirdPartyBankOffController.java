package coopbank.digital.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import coopbank.digital.demo.model.BankHolidaysResponseDTO;
import coopbank.digital.demo.process.ThirdPartyBankOffProcess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/thirdParty/Bank")
@Api(value = "Bank Off Days list Application")
public class ThirdPartyBankOffController {

	@Autowired
	ThirdPartyBankOffProcess thirdPartyBankOffProcess;

	@ApiOperation(value = "Third Party app to provide bank off days list", response = BankHolidaysResponseDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Bank Off days list"),
			@ApiResponse(code = 400, message = "invalid input") })
	@GetMapping("/bankOffDays")
	public ResponseEntity<? extends Object> getThirdPartyBankDayOff
					(@ApiParam(value = "Bank name and Response is Holiday list of this bank", required = false)@RequestParam(name = "bankName", required = true) String bankName) {

		return thirdPartyBankOffProcess.getBankHolidayList(bankName);

	}
}
