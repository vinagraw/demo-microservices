package coopbank.digital.demo.process;

import org.springframework.http.ResponseEntity;

import coopbank.digital.demo.model.BankHolidaysResponseDTO;

public interface ThirdPartyBankOffProcess {

	public ResponseEntity<? extends Object> getBankHolidayList(String bankName);

}
