package coopbank.digital.demo.processimpl;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import coopbank.digital.demo.config.BankHolidayConfig;
import coopbank.digital.demo.model.BankHolidaysResponseDTO;
import coopbank.digital.demo.model.ErrorResponse;
import coopbank.digital.demo.process.ThirdPartyBankOffProcess;

@Component
public class ThirdPartyBankOffProcessImpl implements ThirdPartyBankOffProcess {

	@Autowired
	BankHolidayConfig bankHolidayConfig;
	
	private static final String BAD_REQUEST = "400";

 public ResponseEntity<? extends Object> getBankHolidayList(String bankName) {

		BankHolidaysResponseDTO response = null;
		ErrorResponse errorResponse = null;
		ArrayList<String> bankCoopHolidaysList=null;
		ArrayList<String> bankBarclaysHolidaysList=null;
		String bankCOOPHolidays=bankHolidayConfig.getBankCOOPHolidayList();
		String bankBarclaysHolidays=bankHolidayConfig.getBankBarclaysHolidayList();
		
		switch (bankName) {
		case "COOP":
			bankCoopHolidaysList = new ArrayList<>(Arrays.asList(bankCOOPHolidays.split(":")));
			response = new BankHolidaysResponseDTO(bankCoopHolidaysList, bankBarclaysHolidaysList,bankHolidayConfig.getBankCOOPHolidaysOffDesc());
			return new ResponseEntity<BankHolidaysResponseDTO>(response, HttpStatus.OK);
		case "BARCLAYS":
			bankBarclaysHolidaysList = new ArrayList<>(Arrays.asList(bankBarclaysHolidays.split(":")));
			response = new BankHolidaysResponseDTO(bankCoopHolidaysList, bankBarclaysHolidaysList,bankHolidayConfig.getBankBarclaysHolidaysOffDesc());
			return new ResponseEntity<BankHolidaysResponseDTO>(response, HttpStatus.OK);
		default:
			errorResponse = new ErrorResponse("User input date is invalid date", BAD_REQUEST);
			return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
		}
	}
}
