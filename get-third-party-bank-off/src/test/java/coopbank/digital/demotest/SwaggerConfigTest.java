package coopbank.digital.demotest;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import coopbank.digital.demo.config.BankHolidayConfig;
import coopbank.digital.demo.config.SwaggerConfig;
import coopbank.digital.demo.processimpl.ThirdPartyBankOffProcessImpl;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Vinay Agrawal Swagger Config class provides the necessary
 *         confguration requirement for Swagger report generation
 *
 */

@RunWith(MockitoJUnitRunner.Silent.class)
public class SwaggerConfigTest {

	@InjectMocks
	private SwaggerConfig swaggerConfig;

	//Unit test for getAPI method
	@Test
	public void testGetAPI() {

		Docket docket = swaggerConfig.api();
		assertNotNull(docket);
	}
}
