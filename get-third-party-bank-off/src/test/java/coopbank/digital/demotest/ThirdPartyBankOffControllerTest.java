package coopbank.digital.demotest;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import coopbank.digital.demo.controller.ThirdPartyBankOffController;
import coopbank.digital.demo.model.BankHolidaysResponseDTO;
import coopbank.digital.demo.processimpl.ThirdPartyBankOffProcessImpl;

/**
 * @author Vinay Agrawal
 * 
 *         JUnit Test cases for get third party bank off Controller class
 *         methods
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ThirdPartyBankOffControllerTest {

	@InjectMocks
	private ThirdPartyBankOffController thirdPartyBankOffController;

	@Mock
	private ThirdPartyBankOffProcessImpl thirdPartyBankOffProcess;

	// test the method getThirdPartyBankDayOff
	@Test
	public void testgetThirdPartyBankDayOff() {

		BankHolidaysResponseDTO createBankHolidayResponseDTO = new BankHolidaysResponseDTO();
		ResponseEntity<? extends Object> actualResponse = new ResponseEntity<BankHolidaysResponseDTO>(HttpStatus.OK);
		ResponseEntity<? extends Object> expectedResponse = new ResponseEntity<BankHolidaysResponseDTO>(
				createBankHolidayResponseDTO, HttpStatus.BAD_REQUEST);

		Mockito.doReturn(expectedResponse).when(thirdPartyBankOffProcess).getBankHolidayList("COOP");
		/* call the controller API call */
		actualResponse = thirdPartyBankOffController.getThirdPartyBankDayOff("COOP");
		assertEquals(expectedResponse.toString(), actualResponse.toString());
	}

	public BankHolidaysResponseDTO createBankHolidayResponseDTO() {
		BankHolidaysResponseDTO BankHolidaysResponseDTO = new BankHolidaysResponseDTO();

		ArrayList<String> bankCOOPHolidayList = new ArrayList<>();
		bankCOOPHolidayList.add("2020-01-15");
		bankCOOPHolidayList.add("2020-01-17");
		bankCOOPHolidayList.add("2020-04-29");
		bankCOOPHolidayList.add("2020-04-27");

		ArrayList<String> bankBarclaysHolidayList = new ArrayList<>();
		bankBarclaysHolidayList.add("2020-01-18");
		bankBarclaysHolidayList.add("2020-01-19");
		bankBarclaysHolidayList.add("2020-04-30");
		bankBarclaysHolidayList.add("2020-05-01");

		BankHolidaysResponseDTO.setBankCOOPHolidayList(bankCOOPHolidayList);
		BankHolidaysResponseDTO.setBankBarclaysHolidayList(bankBarclaysHolidayList);
		BankHolidaysResponseDTO.setBankDescription("COOP Bank Holiday List");
		return BankHolidaysResponseDTO;
	}
}
