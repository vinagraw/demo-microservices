package coopbank.digital.demotest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import coopbank.digital.demo.config.BankHolidayConfig;
import coopbank.digital.demo.model.BankHolidaysResponseDTO;
import coopbank.digital.demo.processimpl.ThirdPartyBankOffProcessImpl;

/**
 * @author Vinay Agrawal
 * 
 *JUnit Test cases for Third party process Implementation class
 *
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class ThirdPartyBankProcessImplTest {

	@InjectMocks
	private ThirdPartyBankOffProcessImpl thirdPartyBankOffProcessImpl;

	@Mock
	private BankHolidayConfig bankHolidayConfig;

	// Unit test for getBankHolidayList method
	@Test
	public void getBankHolidayListTest() {

		BankHolidaysResponseDTO response = null;
		ArrayList<String> bankCoopHolidaysList = null;
		ArrayList<String> bankBarclaysHolidaysList = null;
		ArrayList<String> bankCOOPHolidaysList = null;
		when(bankHolidayConfig.getBankCOOPHolidayList()).thenReturn("2020-01-15:2020-01-17:2020-04-29:2020-04-27");
		when(bankHolidayConfig.getBankBarclaysHolidayList())
				.thenReturn("2020-01-18:2020-01-19:2020-04-30:2020-04-27:2020-04-30");
		when(bankHolidayConfig.getBankCOOPHolidaysOffDesc()).thenReturn("COOP Bank Holiday List");
		when(bankHolidayConfig.getBankBarclaysHolidaysOffDesc()).thenReturn("Barclay Bank Holiday List");

		/* Scenario 1: when bankName is COOP */
		response = new BankHolidaysResponseDTO(
				new ArrayList<>(Arrays.asList(bankHolidayConfig.getBankCOOPHolidayList().split(":"))),
				bankBarclaysHolidaysList, bankHolidayConfig.getBankCOOPHolidaysOffDesc());
		ResponseEntity<BankHolidaysResponseDTO> bankCOOPHolidaysResponseDTO = new ResponseEntity(response,
				HttpStatus.OK);

		ResponseEntity<? extends Object> actualBankCOOPHolidaysResponse = thirdPartyBankOffProcessImpl
				.getBankHolidayList("COOP");

		assertEquals(bankCOOPHolidaysResponseDTO.toString(), actualBankCOOPHolidaysResponse.toString());

		/* Scenario 2: when bankName is BARCLAYS */
		response = new BankHolidaysResponseDTO(bankCOOPHolidaysList,
				new ArrayList<>(Arrays.asList(bankHolidayConfig.getBankBarclaysHolidayList().split(":"))),
				bankHolidayConfig.getBankBarclaysHolidaysOffDesc());

		ResponseEntity<BankHolidaysResponseDTO> bankBarclaysHolidaysResponseDTO = new ResponseEntity(response,
				HttpStatus.OK);

		ResponseEntity<? extends Object> actualBankBarclaysHolidaysResponse = thirdPartyBankOffProcessImpl
				.getBankHolidayList("BARCLAYS");

		assertEquals(bankBarclaysHolidaysResponseDTO.toString(), actualBankBarclaysHolidaysResponse.toString());

	}
}
